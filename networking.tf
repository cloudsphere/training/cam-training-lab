data "aws_availability_zones" "available" {}

module "vpc" {
    source              = "terraform-aws-modules/vpc/aws"
    name                = "cloudsphere-lab-vpc"
    cidr                = "10.0.0.0/16"
    azs                 = data.aws_availability_zones.available.names
    private_subnets     = ["10.0.1.0/24"]
    public_subnets      = ["10.0.101.0/24"]
    enable_nat_gateway  = true
    single_nat_gateway  = true
    enable_dns_hostnames = true
    enable_dns_support = true
}

// SG to allow SSH connections from anywhere
resource "aws_security_group" "allow_ssh_pub" {
  name        = "cloudsphere-lab-allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "SSH from the internet"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "http from the internet"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "cloudsphere-lab-allow_ssh_pub"
  }
}

// SG to onlly allow SSH connections from VPC public subnets
resource "aws_security_group" "allow_remote_priv" {
  name        = "cloudsphere-lab-allow_remote_priv"
  description = "Allow inbound traffic from public SG"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "allow all from internal VPC clients"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.0/16"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "cloudsphere-lab-allow_priv"
  }
}