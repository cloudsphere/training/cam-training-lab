// Generate the SSH keypair that we’ll use to configure the EC2 instance. 
// After that, write the private key to a local file and upload the public key to AWS

resource "tls_private_key" "key" {
  algorithm = "RSA"
}

resource "local_sensitive_file" "cloudsphere_lab_private_key" {
  filename          = "cloudsphere-lab-key.pem"
  content = tls_private_key.key.private_key_pem
  file_permission   = "0400"
}
resource "local_sensitive_file" "cloudsphere_lab_ssh_key" {
  file_permission = "0400"
  filename = "cloudsphere-lab-private-key"
  content = tls_private_key.key.private_key_openssh
}

resource "local_sensitive_file" "cloudsphere_lab_public_key" {
  filename          = "cloudsphere-lab-key.pub"
  content = tls_private_key.key.public_key_openssh
  file_permission   = "0400"
}

resource "aws_key_pair" "key_pair" {
  key_name   = "cloudsphere-lab-key"
  public_key = tls_private_key.key.public_key_openssh
}
