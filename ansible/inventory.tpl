[all:vars]
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
ansible_python_interpreter=/usr/bin/python3
ansible_user=ubuntu
ansible_ssh_private_key_file=cloudsphere-lab-private-key

# bastion
[bastion]
bastion-01 ansible_host=${bastion}

# linux Servers
[linux]
cs-lab-linux-0 ansible_host=${linux_0}
cs-lab-linux-1 ansible_host=${linux_1}
cs-lab-linux-2 ansible_host=${linux_2}

[linux:vars]
ansible_ssh_common_args='-o ProxyCommand="ssh  -i cloudsphere-lab-private-key -o StrictHostKeyChecking=no -W %h:%p -q ubuntu@${bastion}" -o StrictHostKeyChecking=no'