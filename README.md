# CAM Training Lab

---

The intended audience for this project is CloudSphere customers and partners who are attending training for our CAM product.
This project uses [Terraform](https://www.terraform.io/), [Ansible](https://www.ansible.com/) and CloudSphere's own Application Simulator to create a small lab in AWS which will be used when scanning with CAM during the training.

***Assumptions***
- In the following documentation, Cloudsphere assume that you do not have the tools installed on your system already. We have provided links to vendor documentation where they can be downloaded and installed on a clean system. If any of these tools exist on your system, you may need to reconfigure them so they are appropriate for use with the training-lab. Their reconfiguration is out of scope for this document.
- The scripts and tools used here have been developed, tested and should be run from a Linux environment. *If you are on a Windows PC, Windows subsystem for Linux should be sufficient for this purpose*.

***DISCLAIMER***
- *While every effort has been made to use free-tier eligible resources for this deployment, CloudSphere can not be held responsible for any costs incurred in any AWS or other cloud-provider account where this terraform configuration is used.*

---

### Pre-requisites

In order to get the most benefit from the training and avoid any delays during the classes you should have the following prepared in advance of your scheduled training dates.

- AWS Account  
You should have access to an AWS account where you are appropriately privileged to create and deploy resources in the eu-west-1 (Ireland) region.

- AWS CLI  
Please follow the [documentation provided by AWS](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) to install AWS CLI.  
Ensure that aws-cli is appropriately configured and logged into the correct AWS account. This can be tested by running the command `aws sts get-caller-identity` and observing the output references the expected account. In a standard environment using the [Quickstart Guide](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-quickstart.html), should be sufficient.

- Terraform  
Please follow the [documentation provided by Hashicorp](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli) to install and configure Terraform

- Ansible
Please follow the [documentation provided by Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) to install and configure Ansible

---

### Steps to create the Lab environment
    
1. Ensure the pre-requisites documented above have been met.
   
2. Navigate to the directory where you have downloaded the repository.

3. Run `terraform init` to initialise the working directory containing the terraform configuration files.
    > this will create a .terraform folder 

4. Run `terraform apply` to execute the configuration and create the resources in the account that aws-cli has been configured for. Terraform will display the resources it is going to add/change/destroy and  ask do you want to perform these actions.
    - Review the output to confirm the changes that terraform is going to make in your AWS account are what is intended. 
        > On first run expect `Plan: 27 to add, 0 to change, 0 to destroy.`
    - Type `yes` to begin the deployment.
        > Terraform will now take some time to deploy the training lab into your configured AWS account. it will display its progress creating the various different resources as it works through them and display an Apply Complete message when it is done.  
        The duration of this process is dependent on the speed of your internet connection, the types of resources being created and the response times from certain AWS services but it should typically complete between 2 and 5 minutes.

5. If the Terraform completes successfully the message `Apply complete! Resources: 27 added, 0 changed, 0 destroyed.` will be displayed on screen and a number of output files will be created, some of these are part of terraform and others are for use later in the course. 
   1. `cloudsphere-lab-key.pem` - PEM encoded RSA private key also stored in AWS as a key-pair
   2. `cloudsphere-lab-key.pub` - SSH Public Key
   3. `cloudsphere-lab-private-key` - SSH Private Key
   4. `credentials.csv` - Credentials and IP Addresses for systems created in the lab
   5. `ansible/hosts.ini` - Ansible inventory file  
    >Some additional terraform specific files and folders will be created; these files are required as part of the lab clean-up at the end of the course so should not be modified or deleted.

6. The next step in standing up the lab is to use Ansible to configure the linux hosts with the cloudsphere-application-simulator. The following command should be run in the same directory where the repository has been downloaded to. `ansible-playbook -i ./ansible/hosts.ini ./ansible/ansible-linux.yaml`

---

### Repo File Listing

`ansible/ansible-linux.yaml`  
Ansible playbook to install and configure app-topology-simulator and its pre-requisites to emulate an ELK stack on 3 linux machines.

`ansible/inventory.tpl`  
Ansible inventory template used as part of the terraform outputs to generate the ansible inventory `ansible/hosts.ini` file.

`.gitignore`
Tells Git which files it should **NOT** track.

`linux.tf`
- Creates a single bastion host in the public subnet with port 22 and 80 open to the internet.
- Creates the number of linux ec2 instances specified in the `variable "instance_count"` of `vars.tf` in the private subnet.
- Associates the ssh key created in `ssh.tf` with all instances

`networking.tf`
- Creates VPC with 
    - Elastic ip
    - NAT Gateway
    - Internet Gateway
    - 1 public subnet `10.0.101.0/24`
    - 1 private subnet `10.0.1.0/24`
    - 1 elastic IP for NAT gateway
- Creates 2 Security Groups which 
    - Allow ssh and http access from the internet to hosts in the public subnet.
    - Allows all communication between the public to the private subnets.

`outputs.tf`
- Creates 2 files used elsewhere during the training course
  - `ansible/hosts.ini` - Ansible inventory file
  - `credentials.csv` - List of System Names, IP Addresses and Credentials required to connect.

`provider.tf`
- Terraform provider configuration 

`README.MD`
- This file

`ssh-key.tf`
- Creates a tls private key and stores portions in 
    - 3 local files
        - cloudsphere-lab-key.pem (RSA private key)
        - cloudsphere-lab-key.pub (SSH public key)
        - cloudsphere-lab-private-key (SSH private key)
- Copies the SSH public key to AWS EC2 as a new key-pair.

`vars.tf`
Sets up the following variables which can be modified as required
- **aws_profile** - The name of the `aws cli` profile on the local machine to be used during the terraform deployment.
- **aws_region** - The AWS region where resources should be deployed. (Note, if you wish to change region, you will also need to change the AMI's specified in the linux_ami and windows_ami variables).
- **instance_count** - the number of linux targets to be deployed
- **instance_type** - the AWS Instance type (size)
- **linux_ami** - The AMI to use for the linux systems (Note that this is specific to the eu-west-1 region)
- **windows_ami** - The AMI to use for the windows systems (Note that this is specific to the eu-west-1 region)

`windows.tf`
- Creates 2 windows ec2 instances in the private subnet.
  -  `cs-lab-win-1` Has SQL Server Express edition installed in default configuration
  -  `cs-lab-win-2` Has SQL Server Express edition emulated by CloudSphere Application Simulator

---

### Clean up

After the training has completed you will want to destroy the resources that terraform has created in your AWS account. This can be completed by navigating to this directory and running the command `terraform destroy`  
Terraform will review the configuration in this repository and the configuration of the corresponding resources in your AWS account and ask if you are sure that you want to destroy all resources. You should type `yes` when prompted.

> Terraform will now take some time to destroy the training lab targets from your configured AWS account. It will display its progress deleting the various different resources as it works through them and display an Apply Complete message when it is done.
The duration of this process is dependent on the speed of your internet connection, the types of resources being destroyed and the response times from certain AWS services but it should typically complete between 3 and 6 minutes.

Note: This will not destroy any manually deployed or configured resources in your AWS account such as any CloudSphere CAM Appliance(s) used during the training, these should be deleted manually as per your trainers instructions.