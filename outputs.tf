resource "local_sensitive_file" "ansible_inventory" {
  filename = "./ansible/hosts.ini"
  content = templatefile("./ansible/inventory.tpl",
  {
    bastion = aws_instance.cloudsphere_bastion[0].public_ip
    linux_0 = aws_instance.cloudsphere-linux[0].private_ip
    linux_1 = aws_instance.cloudsphere-linux[1].private_ip
    linux_2 = aws_instance.cloudsphere-linux[2].private_ip
  })
  file_permission = "0400"
}

# terraform output --json | jq 'with_entries(.value |= .value)'
resource "local_sensitive_file" "credentials" {
  content = <<-DOC
  Hostname, IPAddress, Username, Password
  ${aws_instance.cloudsphere_bastion[0].tags_all.Name}, ${aws_instance.cloudsphere_bastion[0].public_ip}, ubuntu, ssh-key: cloudsphere-lab-private-key
  ${aws_instance.cloudsphere-linux[0].tags_all.Name}, ${aws_instance.cloudsphere-linux[0].private_ip}, ubuntu, ssh-key: cloudsphere-lab-private-key
  ${aws_instance.cloudsphere-linux[1].tags_all.Name}, ${aws_instance.cloudsphere-linux[1].private_ip}, ubuntu, ssh-key: cloudsphere-lab-private-key
  ${aws_instance.cloudsphere-linux[2].tags_all.Name}, ${aws_instance.cloudsphere-linux[2].private_ip}, ubuntu, ssh-key: cloudsphere-lab-private-key
  ${aws_instance.cs-lab-win-1.tags_all.Name}, ${aws_instance.cs-lab-win-1.private_ip}, administrator, ${rsadecrypt(aws_instance.cs-lab-win-1.password_data, tls_private_key.key.private_key_pem)}
  ${aws_instance.cs-lab-win-2.tags_all.Name}, ${aws_instance.cs-lab-win-2.private_ip}, administrator, ${rsadecrypt(aws_instance.cs-lab-win-2.password_data, tls_private_key.key.private_key_pem)}
  DOC
  filename = "./credentials.csv"
}