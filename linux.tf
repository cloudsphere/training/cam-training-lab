resource "aws_instance" "cloudsphere-linux" {
    count = var.instance_count

    ami             = var.linux_ami
    instance_type   = var.instance_type
    subnet_id       = module.vpc.private_subnets[0]
    vpc_security_group_ids = [aws_security_group.allow_remote_priv.id]
    
    key_name = aws_key_pair.key_pair.key_name

    tags = {
        Name    = "cs-lab-linux-${count.index}"
    }
}

resource "aws_instance" "cloudsphere_bastion" {
    count = 1

    ami             = var.linux_ami
    instance_type   = var.instance_type
    subnet_id       = module.vpc.public_subnets[0]
    vpc_security_group_ids = [aws_security_group.allow_ssh_pub.id]
    
    key_name = aws_key_pair.key_pair.key_name

    tags = {
        Name    = "cs-bastion-${count.index}"
    }
}