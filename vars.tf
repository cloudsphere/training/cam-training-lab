variable "aws_profile" {
  default = "default"
}
variable "aws_region" {
  default = "eu-west-1"
  # default = "us-east-1"
}
variable "instance_count" {
  default = "3"
}
variable "instance_type" {
  default = "t2.micro"
}
variable "linux_ami" {
  default = "ami-0f9ae27ecf629cbe3" # ubuntu 22.04 amd64 eu-west-1
  # default = "ami-007855ac798b5175e" # ubuntu 22.04 amd64 us-east-1
}
variable "windows_ami" {
  default = "ami-0a4425a3b85ad26df" # Windows Server 2022 Core Base eu-west-1
  # default = "ami-03a21b62905737826" # Windows Server 2022 Core Base us-east-1
}
