resource "aws_instance" "cs-lab-win-1" {
    ami             = var.windows_ami
    instance_type   = var.instance_type
    subnet_id       = module.vpc.private_subnets[0]
    vpc_security_group_ids = [aws_security_group.allow_remote_priv.id]
    
    key_name = aws_key_pair.key_pair.key_name
    get_password_data     =   "true"

    user_data = <<EOF
<powershell>
  Invoke-WebRequest -Uri https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1 -OutFile ConfigureRemotingForAnsible.ps1 
  powershell -ExecutionPolicy RemoteSigned .\ConfigureRemotingForAnsible.ps1
  Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
  choco install sql-server-express -y
</powershell>
    EOF

    tags = {
        Name    = "cs-lab-win-1"
    }
}

resource "aws_instance" "cs-lab-win-2" {
    ami             = var.windows_ami
    instance_type   = var.instance_type
    subnet_id       = module.vpc.private_subnets[0]
    vpc_security_group_ids = [aws_security_group.allow_remote_priv.id]
    
    key_name = aws_key_pair.key_pair.key_name
    get_password_data     =   "true"

    user_data = <<EOF
<powershell>
  Invoke-WebRequest -Uri https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1 -OutFile ConfigureRemotingForAnsible.ps1 
  powershell -ExecutionPolicy RemoteSigned .\ConfigureRemotingForAnsible.ps1
  Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
  choco install jdk8 -y
  Invoke-WebRequest -Uri https://s3.amazonaws.com/downloads.cloudsphere.com/cam-training/app-topology-simulator-1.0.3-jar-with-dependencies.jar -OutFile c:\app-topology-simulator.jar
  'C:\Program Files (x86)\Common Files\Oracle\Java\javapath\java.exe -jar c:\app-topology-simulator.jar --fingerprint=CloudsphereDemo --id=1 --model=MicrisoftSQL --vendor=Microsoft --version=8.6.0 --type=DatabaseServer --servicename=MSSQLEXPRESS --servicelabel=MSSQLEXPRESS --port=1433'
</powershell>
    EOF

    tags = {
        Name    = "cs-lab-win-2"
    }
}